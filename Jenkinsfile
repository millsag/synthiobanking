pipeline {
  agent any
  tools {
       maven 'maven3'
  }
  options {
    timeout(time: 15, unit: 'MINUTES')
    buildDiscarder(logRotator(numToKeepStr: '5')) 
  }
  stages {
    stage('Build') {
      steps {
        sh 'mvn clean install'
      }
    }
    stage('Code Quality Preview Scan') {
     when {
        branch 'PR*'
      }
      steps {
          withSonarQubeEnv('SonarQube') {
            sh 'mvn clean package sonar:sonar -Dsonar.analysis.mode=issues'
          }
      }
    }  
    stage('Code Quality Full Scan') {
     when {
        branch 'master'
      }
      steps {
          withSonarQubeEnv('SonarQube') {
            sh 'mvn clean package sonar:sonar'
          }
      }
    }  
    stage('Unit Test') {
      steps {
        sh 'mvn -Dmaven.test.failure.ignore=true install'
      }
       post {
         success {
           junit 'target/surefire-reports/**/*.xml' 
         }
       }
    }
    stage('deploy to Nexus') {
      when {
        branch 'master'
      }
      steps {
        sh 'mvn clean deploy -Dmaven.test.skip=true'
      }
    }
  }
  post {
    success {
      slackSend (color: '#00FF00', message: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
    }

    failure {
      slackSend (color: '#FF0000', message: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
    }
  }
}  