package com.synthio.maven.synthioBanking;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name="synthioBanking", urlPatterns = "/processBanking")
public class CustomerController extends HttpServlet {

	private Account account;
	private User user;
	int amount;
	
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
    	String stringAmount = request.getParameter("amount");
    	if (stringAmount!=null && stringAmount.length()>0) {
    		try {
    		 amount = Integer.parseInt(request.getParameter("amount"));
    		}
    		catch(NumberFormatException nfe) {
    			//this never happened
    		}
    	}
    	else {
    		amount = 0;
    	}
    	
    	if (user == null) {
            user = new User();
        }
        if (account == null) {
            account = new Account();
            user.setAccount(account);
        }

        String action = request.getParameter("ATMAction");
        
        if(action.equals("Withdrawal")) {
        	account.withdraw(amount);
        }
        
        if(action.equals("Deposit")) {
        	account.deposit(amount);
        }
        
        request.setAttribute("balance", account.getBalance());
        String url = "/";
        forwardResponse(url, request, response);
    }



    private void forwardResponse(String url, HttpServletRequest request, HttpServletResponse response) {
        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    
        

        
    
}
