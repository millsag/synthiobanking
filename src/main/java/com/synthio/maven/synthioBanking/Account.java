package com.synthio.maven.synthioBanking;

public class Account {

	private int balance;

	public Account() {
		this.balance = 0;
	}

	public Account(int initialDeposit) {
		this.balance = initialDeposit;
	}
	
	public int getBalance() {
		return balance;
	}

	public void deposit(int amount) {
		this.balance += amount;
	}

	public void withdraw(int amount) {
		this.balance -= amount;
	}
	
}
	
