<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Synthio ATM</title>
</head>
<body>
    <h1>World's most basic ATM version 2!</h1>

    

    <form method="post"  action="${pageContext.request.contextPath}/processBanking" >
     
        <label for="amount">Amount : </label>
        <input type="text" name="amount" id="amount" value="${amount}">
        <input type="submit" name="ATMAction" value="Balance">
        <input type="submit" name="ATMAction" value="Withdrawal">
        <input type="submit" name="ATMAction" value="Deposit">
        <p>
        <p>
        <p><strong>Balance: </strong> ${balance}</p>
    </form>
</body>
</html>