package com.synthio.maven.synthioBanking;

import org.junit.Test;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
//import synthioBank.Account;
//import synthioBank.User;


public class TestAccount {

	
	@Test
	public void testInitialDeposit() {
		Account account = new Account(100);
		User user = new User();
		user.setAccount(account);
		assertEquals(100, account.getBalance());
	}

    @Test
    public void testDeposit() {
		Account account = new Account();
		User user = new User();
		user.setAccount(account);
    	account.deposit(100);
    	assertEquals(100, account.getBalance());
    }

    @Test
    public void testWithdrawal() {
    	Account account = new Account(100);
		User user = new User();
		user.setAccount(account);
    	account.withdraw(34);
    	assertEquals(66, account.getBalance());
    }
}
