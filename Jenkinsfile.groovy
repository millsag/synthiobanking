node {
   def mvnHome
   stage('Checkout') { // for display purposes
      // Get some code from a GitHub repository
      git 'https://millsag@bitbucket.org/millsag/synthiobanking.git'
      // Get the Maven tool.
      // ** NOTE: This 'M3' Maven tool must be configured
      // **       in the global configuration.           
      mvnHome = tool 'maven'
      def pom = readMavenPom file: "pom.xml"
      version = pom.version.replace("0-SNAPSHOT", "${currentBuild.number}")
      println(version)
   }
   stage('Build/UnitTest') {
      // Run the maven build
      sh "${mvnHome}/bin/mvn versions:set -DnewVersion=${version}"
      try{
        sh "${mvnHome}/bin/mvn  clean install"
      // sh "${mvnHome}/bin/mvn -DreleaseVersion=${version} -DdevelopmentVersion=${pom.version} -Dmaven.test.failure.ignore clean package"
      }
      catch(err) {
        step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/TEST-*.xml'])
        if (currentBuild.result == 'UNSTABLE')
            currentBuild.result = 'FAILURE'
        throw err
      }
     // sh "git push ${pm.artifactId}-${version}"
   }
   stage('Code Quality') {
       
      //holding area for sonar scan
   }
   stage('Git Tag') {

      withCredentials([[$class: 'UsernamePasswordMultiBinding', 
                credentialsId: 'bitbucketURLEncoded', 
                usernameVariable: 'GIT_USERNAME', 
                passwordVariable: 'GIT_PASSWORD']]) {    
             sh "git tag ${version}"
             sh("git push 'https://${GIT_USERNAME}:${GIT_PASSWORD}@bitbucket.org/millsag/synthiobanking.git' ${version}")
           
            }
    
    }
   
   stage('Push to Nexus'){
     sh "'${mvnHome}/bin/mvn' clean deploy -Dmaven.test.skip=true"
   }
  
   
   stage('Deploy'){
     
    repo="http://172.42.98.172:8081"
    groupId="com/synthio"
    artifactId="synthioBanking"
    type="war"

    filename="${artifactId}-${version}.${type}"

    sh "wget --no-check-certificate '${repo}/repository/maven-releases/${groupId}/${artifactId}/${version}/${filename}' -O /tmp/synthioBanking.war -k"
  
    //now push to the server
    sh "scp  /tmp/synthioBanking.war ubuntu@172.42.36.110:/opt/tomcat/webapps/"
    
    //now remote in to server and move file
  //  sh "ssh ubuntu@172.42.36.110"
    //sh "cd /opt/tomcat/webapps"
  //  sh "sudo cp /tmp/synthioBanking.war /opt/tomcat/webapps/"
    }
    
    
   stage('Integration Tests'){
       //holding area for integration tests
   }
   
    
}